recipes.addShaped(<Thaumcraft:ItemEldritchObject:3>,
 [[<Botania:manaResource:14>, <Thaumcraft:ItemEldritchObject>, <appliedenergistics2:item.ItemSpatialStorageCell.128Cubed>],
  [<Thaumcraft:ItemEldritchObject>, <Botania:dice>, <Thaumcraft:ItemEldritchObject>],
  [<appliedenergistics2:item.ItemSpatialStorageCell.128Cubed>, <Thaumcraft:ItemEldritchObject>, <Botania:manaResource:14>]]);
recipes.addShaped(<Thaumcraft:ItemEldritchObject:3>,
 [[<appliedenergistics2:item.ItemSpatialStorageCell.128Cubed>, <Thaumcraft:ItemEldritchObject>, <Botania:manaResource:14>],
  [<Thaumcraft:ItemEldritchObject>, <Botania:dice>, <Thaumcraft:ItemEldritchObject>],
  [<Botania:manaResource:14>, <Thaumcraft:ItemEldritchObject>, <appliedenergistics2:item.ItemSpatialStorageCell.128Cubed>]]);